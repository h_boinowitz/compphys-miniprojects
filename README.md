# Miniprojects CompPhys

Unsere Miniprojekte aus den Lehrveranstaltungen "Computational Physics" und "Computational Physics II".

Diese Projekte wurden mit und für Kaggle entwickelt. Sie sind teilweise *nicht* lokal lauffähig.

## Projekte
- Perkolierende Cluster
- Autoencoder
- Restricted Boltzmann Machine
- Bloch Vektoren